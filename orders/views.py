from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.


def orders_list(request):
    return render(request, "orders/orders_list.html")


def carts_list(request):
    return render(request, "orders/carts_list.html")


def promotion(request):
    return render(request, "orders/promotion.html")

