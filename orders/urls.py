__author__ = 'db'
from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
                       url(r'^orders_list$', 'orders.views.orders_list', name='orders_list'),
                       url(r'^carts_list$', 'orders.views.carts_list', name='carts_list'),
                       url(r'^promotion$', 'orders.views.promotion', name='promotion'),
)
