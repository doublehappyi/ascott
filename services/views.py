from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.
from services.models import City, Brand, Apartment,Room


def service(request):
    city_list = City.objects.all()
    brand_list = Brand.objects.all()
    apartment_list = Apartment.objects.all()

    try:
        brand_id = int(request.GET["brand_id"])
        city_id = int(request.GET["city_id"])

        if brand_id == 0:
            brand_selected = brand_list
        else:
            brand_selected = brand_list.filter(id=brand_id)

        if city_id == 0:
            city_selected = city_list
        else:
            city_selected = city_list.filter(id=city_id)
    except:
        brand_id = 0
        city_id = 0
        city_selected = city_list
        brand_selected = brand_list

    group_list = []

    if city_selected == city_list and brand_selected == brand_list:
        for city in city_selected:
            if len(apartment_list.filter(city=city.id)) > 0:
                group_list.append({
                    "city_name": city.name,
                    "apartment_list": apartment_list.filter(city=city.id)
                })
    elif city_selected == city_list and brand_selected != brand_list:
        for city in city_selected:
            if len(apartment_list.filter(city=city.id, brand=brand_selected[0].id)) > 0:
                group_list.append({
                    "city_name": city.name,
                    "apartment_list": apartment_list.filter(city=city.id, brand=brand_selected[0].id)
                })
    elif city_selected != city_list and brand_selected == brand_list:
        for city in city_selected:
            if len(apartment_list.filter(city=city.id)) > 0:
                group_list.append({
                    "city_name": city.name,
                    "apartment_list": apartment_list.filter(city=city.id)
                })
    elif city_selected != city_list and brand_selected != brand_list:
        for city in city_selected:
            if len(apartment_list.filter(city=city.id, brand=brand_selected[0].id)) > 0:
                group_list.append({
                    "city_name": city.name,
                    "apartment_list": apartment_list.filter(city=city.id, brand=brand_selected[0].id)
                })
    else:
        pass

    # print group_list

    context = {
        "city_list": city_list,
        "brand_list": brand_list,
        "group_list": group_list,
        "brand_id": brand_id,
        "city_id": city_id
    }
    return render(request, "services/service.html", context)


def moment(request):
    return render(request, "services/moment.html")


def service_with(request):
    id = int(request.GET["id"])
    apartment = Apartment.objects.filter(id=id)[0]

    room_list = Room.objects.filter(apartment=id)
    context = {
        "apartment":apartment,
        "room_list":room_list
    }
    return render(request, "services/service_with.html",context)




