# coding=utf-8
from django.db import models
import time
import datetime
# Create your models here.

# 国家
class Country(models.Model):
    name = models.CharField(max_length=20, unique=True, verbose_name="国家名字")

    class Meta:
        verbose_name_plural = "国家列表"

    def __unicode__(self):
        return self.name


# 城市
class City(models.Model):
    class Meta:
        verbose_name_plural = "城市列表"

    name = models.CharField(max_length=20, unique=True, verbose_name="城市名字")
    country = models.ForeignKey(Country)

    def __unicode__(self):
        return self.name


# 品牌
class Brand(models.Model):
    class Meta:
        verbose_name_plural = "品牌列表"

    name = models.CharField(max_length=20, unique=True, verbose_name="品牌名字")

    def __unicode__(self):
        return self.name


# 公寓/酒店
class Apartment(models.Model):
    class Meta:
        verbose_name_plural = "公寓列表"

    # 酒店名字，开业状态，酒店简介，酒店详细介绍，酒店公告
    name = models.CharField(max_length=20)
    STATUS = ((0, "未开业"), (1, "即将开业"), (2, "新开业"), (4, "已开业"))
    status = models.IntegerField(choices=STATUS, verbose_name="营业状态")
    desc = models.TextField(max_length=200, verbose_name="公寓简介")
    address = models.CharField(max_length=200, unique=True, verbose_name="公寓地址")
    telephone = models.CharField(max_length=13, verbose_name="公寓电话")
    fax = models.CharField(max_length=100, verbose_name="公寓传真")
    introduction = models.TextField(max_length=500, verbose_name="公寓详细介绍")
    notice = models.TextField(max_length=200, verbose_name="注意事项")
    # 外键
    brand = models.ForeignKey(Brand, verbose_name="所属公寓品牌")
    city = models.ForeignKey(City, verbose_name="所在城市")

    def __unicode__(self):
        return self.name


# 房间
class Room(models.Model):
    class Meta:
        verbose_name_plural = "房间列表"

    # 入住成人数，入住儿童数，价钱（美元，人民币），到达时间，离开时间，房晚，所属公寓id
    number = models.CharField(max_length=100, verbose_name="房间号码")
    type = models.CharField(max_length=50, verbose_name="房型")
    type_image = models.CharField(max_length=200, verbose_name="户型图片")
    desc = models.TextField(max_length=200, verbose_name="描述")
    OCCUPANCY = ((0, "未入住"), (1, "已入住"))
    is_occupancy = models.IntegerField(choices=OCCUPANCY, verbose_name="入住情况")

    arrive_time = models.DateTimeField(auto_now=False, verbose_name="到达时间")
    leave_time = models.DateTimeField(auto_now=False, verbose_name="离开时间")
    nights = models.SmallIntegerField(max_length=30, verbose_name="房晚")
    price = models.SmallIntegerField(max_length=10000, verbose_name="价格/（元）")
    apartment = models.ForeignKey(Apartment, verbose_name="所属公寓")

    def __unicode__(self):
        return str(self.id) + "_" + str(self.number)


# 儿童
class Child(models.Model):
    class Meta:
        verbose_name_plural = "儿童列表"

    name = models.CharField(max_length=100, verbose_name="儿童名字")
    AGE = ((0, "0岁"), (1, "1岁"), (2, "2岁"))
    age = models.IntegerField(choices=AGE, verbose_name="年龄")
    room = models.ForeignKey(Room, verbose_name="所住房间号码")

    def __unicode__(self):
        return self.name


# 成人
class Adult(models.Model):
    class Meta:
        verbose_name_plural = "成人列表"

    name = models.CharField(max_length=20, verbose_name="名字")
    room = models.ForeignKey(Room, verbose_name="所住房间号码")

    def __unicode__(self):
        return self.name