#coding=utf-8
from django.contrib import admin
from .models import Country, City, Brand, Apartment, Room, Child, Adult
# Register your models here.
class CountryAdmin(admin.ModelAdmin):
    class meta:
        model = Country


admin.site.register(Country, CountryAdmin)


class CityAdmin(admin.ModelAdmin):
    class meta:
        model = City


admin.site.register(City, CityAdmin)


class BrandAdmin(admin.ModelAdmin):
    class meta:
        model = Brand


admin.site.register(Brand, BrandAdmin)


class ApartmentAdmin(admin.ModelAdmin):
    class meta:
        model = Apartment


admin.site.register(Apartment, ApartmentAdmin)


class RoomAdmin(admin.ModelAdmin):
    class meta:
        model = Room


admin.site.register(Room, RoomAdmin)


class ChildAdmin(admin.ModelAdmin):
    class meta:
        model = Child


admin.site.register(Child, ChildAdmin)


class AdultAdmin(admin.ModelAdmin):
    class meta:
        model = Adult


admin.site.register(Adult, AdultAdmin)