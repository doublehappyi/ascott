from django.conf.urls import patterns, include, url

from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
                       # Examples:
                       # url(r'^$', 'main.views.home', name='home'),
                       # url(r'^blog/', include('blog.urls')),

                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^$', 'main.views.home', name='home'),
                       url(r'^home$', 'main.views.home', name='home'),
                       url(r'^orders/', include('orders.urls')),
                       url(r'^users/', include('users.urls')),
                       url(r'^services/', include('services.urls')),
                       url(r'^company/', include('company.urls')),
)
