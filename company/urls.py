__author__ = 'db'
from django.conf.urls import patterns, include, url

urlpatterns = patterns('',
                       url(r'^about_us$', 'company.views.about_us', name='about_us'),
                       url(r'^about_brand$', 'company.views.about_brand', name='about_brand'),
                       url(r'^about_honor$', 'company.views.about_honor', name='about_honor'),
                       url(r'^about_duty$', 'company.views.about_duty', name='about_duty'),
                       url(r'^about_ascott$', 'company.views.about_ascott', name='about_ascott'),
                       url(r'^about_recruit$', 'company.views.about_recruit', name='about_recruit'),
                       url(r'^about_card$', 'company.views.about_card', name='about_card'),
                       url(r'^cooperation$', 'company.views.cooperation', name='cooperation'),
)
