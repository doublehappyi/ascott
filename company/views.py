from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.


def about_us(request):
    return render(request, "company/about_us.html")


def about_brand(request):
    return render(request, "company/about_brand.html")


def about_honor(request):
    return render(request, "company/about_honor.html")


def about_duty(request):
    return render(request, "company/about_duty.html")


def about_ascott(request):
    return render(request, "company/about_ascott.html")


def about_recruit(request):
    return render(request, "company/about_recruit.html")


def about_card(request):
    return render(request, "company/about_card.html")


def cooperation(request):
    return render(request, "company/cooperation.html")